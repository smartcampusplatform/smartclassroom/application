$('document').ready(function () {

  var question = [];

  /**
    * Link
    */
  $('#lecturerDashboardLink').on('click', function () {
    location.href = 'lecturer_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + $.urlParam('u');
  });
  $('#lecturerClassMeetingLink').on('click', function () {
    location.href = 'lecturer_class_meeting.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_meeting.html?u=' + $.urlParam('u');
  });
  $('#lecturerCreateAssignmentLink').on('click', function () {
    location.href = 'lecturer_create_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_create_assignment.html?u=' + $.urlParam('u');
  });
  $('#lecturerTaskDetailLink').on('click', function () {
    location.href = 'lecturer_task_detail.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_task_detail.html?u=' + $.urlParam('u');
  });
  $('#lecturerCourseManagementLink').on('click', function () {
    location.href = 'lecturer_course_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_management.html?u=' + $.urlParam('u');
  });
  $('#learningOutcomeManagementLink').on('click', function () {
    location.href = 'learning_outcome_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/learning_outcome_management.html?u=' + $.urlParam('u');
  });

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 2) {
            $('#userId').html(res.data.login_id);
            $('#submitPopUpBtn').hide();
            $('#addquestion').hide();
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    }).done(function () {

      /**
        * pop up question
        */
      $.ajax({
        url: 'http://167.205.59.43:8011/courseclass/getcourseclass?learningPeriodId=' + new Date().getFullYear() + '-1&staffId=' + $('#userId').html(),
        type: 'GET',
        encode: true,
        success: function (res) {
          // console.log(res);
          if (res.message.length > 0) {
            for (var i = 0; i < res.message.length; i++) {
              $('#classChoosen').append('<option name=\"' + res.message[i].courseOfferId + '\" value=\"' + res.message[i].courseOfferId + '\"> &nbsp;' + res.message[i].courseNameId + '</option>');
              $('#classChoosenAss').append('<option name=\"' + res.message[i].courseOfferId + '\" value=\"' + res.message[i].courseOfferId + '\"> &nbsp;' + res.message[i].courseNameId + '</option>');
              $('#classChoosenQuiz').append('<option name=\"' + res.message[i].courseOfferId + '\" value=\"' + res.message[i].courseOfferId + '\"> &nbsp;' + res.message[i].courseNameId + '</option>');
        	  }
          }
        },
        error: function (err) {
          console.log('error: ', err);
          // alert('error: ', err);
        }
      }).done(function () {

        $('#classChoosen').on('change', function (evt) {

          $('#subTopicList').empty();

          /**
            * get sub topic
            */
          $.ajax({
            url: 'http://167.205.59.43:8011/courseclass/getsubtopicmeeting?courseOfferId=' + $(this).val(),
            type: 'GET',
            encode: true,
            success: function (res) {
              // console.log(res);
              if (res.message.length > 0) {
              	for (var i = 0; i < res.message.length; i++) {
                  $('#subTopicList').append('<option name=\"' + res.message[i].courseSubTopicId + '\" value=\"' + res.message[i].courseSubTopicId + '\"> &nbsp;' + res.message[i].subTopicTitle + '</option>');
              	}
                $('#addquestion').show();
              } else {
                $('#addquestion').hide();
              }
            },
            error: function (err) {
              console.log('error: ', err);
              // alert('error: ', err);
            }
          }).done(function () {

            $("#addquestion").on('click', function (evt) {

              $('#formQuestionPopUp').empty();
              $('#formQuestionPopUp').append('<div class=\"description-text\"><h6 id=\"questionnum\" class=\"popupquestion\">Question #' + (question.length + 1) + '</h6></div>');
              $('#formQuestionPopUp').append('<div class=\"form-group\"><label for=\"questionFormControlInput\">Question</label><input type=\"text\" class=\"form-control\" id=\"questionFormControlInput\" placeholder=\"Fill the question here\"></div>');
              $('#formQuestionPopUp').append('<div class=\"form-group\"><label for=\"optaFormControlInput\">Option A</label><input type=\"text\" class=\"form-control\" id=\"optaFormControlInput\" placeholder=\"Fill the option A here\"></div>');
              $('#formQuestionPopUp').append('<div class=\"form-group\"><label for=\"optbFormControlInput\">Option B</label><input type=\"text\" class=\"form-control\" id=\"optbFormControlInput\" placeholder=\"Fill the option B here\"></div>');
              $('#formQuestionPopUp').append('<div class=\"form-group\"><label for=\"optcFormControlInput\">Option C</label><input type=\"text\" class=\"form-control\" id=\"optcFormControlInput\" placeholder=\"Fill the option C here\"></div>');
              $('#formQuestionPopUp').append('<div class=\"form-group\"><label for=\"optdFormControlInput\">Option D</label><input type=\"text\" class=\"form-control\" id=\"optdFormControlInput\" placeholder=\"Fill the option D here\"></div>');
              $('#formQuestionPopUp').append('<div class=\"form-group\"><label for=\"opteFormControlInput\">Option E</label><input type=\"text\" class=\"form-control\" id=\"opteFormControlInput\" placeholder=\"Fill the option E here\"></div>');
              $('#formQuestionPopUp').append('<div class=\"form-group\"><label for=\"ansFormControlInput\">Answer</label><select class=\"form-control\" id=\"ansFormControlInput\"><option value=\"A\">Option A</option><option value=\"B\">Option B</option><option value=\"C\">Option C</option><option value=\"D\">Option D</option><option value=\"E\">Option E</option></select></div>');

            });

            $("#saveQuestion").on('click', function (evt) {

              $('#popupModalLive').modal('toggle');

              question.push({
                coursesubtopicid: $('#subTopicList').val(),
                question: $('#questionFormControlInput').val(),
                optiona: $('#optaFormControlInput').val(),
                optionb: $('#optbFormControlInput').val(),
                optionc: $('#optcFormControlInput').val(),
                optiond: $('#optdFormControlInput').val(),
                optione: $('#opteFormControlInput').val(),
                correctanswer: $('#ansFormControlInput').val(),
                staffid: $('#userId').html(),
                statusactive: 1
              });

              $('#questioncontainer').append('<h3>' + $('#questionnum').html() + '</h3>');
              if ($('#ansFormControlInput').val() == 'A') {
                $('#questioncontainer').append('<div><p>' + $('#questionFormControlInput').val() + '</p><ul><li type=\"A\" class=\"bg-success\">' + $('#optaFormControlInput').val() + '</li><li type=\"A\">' + $('#optbFormControlInput').val() + '</li><li type=\"A\">' + $('#optcFormControlInput').val() + '</li><li type=\"A\">' + $('#optdFormControlInput').val() + '</li><li type=\"A\">' + $('#opteFormControlInput').val() + '</li></ul></div>');
              }
              if ($('#ansFormControlInput').val() == 'B') {
                $('#questioncontainer').append('<div><p>' + $('#questionFormControlInput').val() + '</p><ul><li type=\"A\">' + $('#optaFormControlInput').val() + '</li><li type=\"A\" class=\"bg-success\">' + $('#optbFormControlInput').val() + '</li><li type=\"A\">' + $('#optcFormControlInput').val() + '</li><li type=\"A\">' + $('#optdFormControlInput').val() + '</li><li type=\"A\">' + $('#opteFormControlInput').val() + '</li></ul></div>');
              }
              if ($('#ansFormControlInput').val() == 'C') {
                $('#questioncontainer').append('<div><p>' + $('#questionFormControlInput').val() + '</p><ul><li type=\"A\">' + $('#optaFormControlInput').val() + '</li><li type=\"A\">' + $('#optbFormControlInput').val() + '</li><li type=\"A\" class=\"bg-success\">' + $('#optcFormControlInput').val() + '</li><li type=\"A\">' + $('#optdFormControlInput').val() + '</li><li type=\"A\">' + $('#opteFormControlInput').val() + '</li></ul></div>');
              }
              if ($('#ansFormControlInput').val() == 'D') {
                $('#questioncontainer').append('<div><p>' + $('#questionFormControlInput').val() + '</p><ul><li type=\"A\">' + $('#optaFormControlInput').val() + '</li><li type=\"A\">' + $('#optbFormControlInput').val() + '</li><li type=\"A\">' + $('#optcFormControlInput').val() + '</li><li type=\"A\" class=\"bg-success\">' + $('#optdFormControlInput').val() + '</li><li type=\"A\">' + $('#opteFormControlInput').val() + '</li></ul></div>');
              }
              if ($('#ansFormControlInput').val() == 'E') {
                $('#questioncontainer').append('<div><p>' + $('#questionFormControlInput').val() + '</p><ul><li type=\"A\">' + $('#optaFormControlInput').val() + '</li><li type=\"A\">' + $('#optbFormControlInput').val() + '</li><li type=\"A\">' + $('#optcFormControlInput').val() + '</li><li type=\"A\">' + $('#optdFormControlInput').val() + '</li><li type=\"A\" class=\"bg-success\">' + $('#opteFormControlInput').val() + '</li></ul></div>');
              }

              // console.log('question length: ' + question.length);

              $('#submitPopUpBtn').show();
              $('#questioncontainer').accordion("refresh");

            });

            /**
              * submit question
              */
            $("#submitQuestion").on('click', function (evt) {

              console.log('question length: ' + question.length);
              var next;
              // console.log('question: ' + JSON.stringify(question));
              if (question.length > 0) {
                var count = 0
                next = function () {
                  if (count < question.length) {
                    updateToServer(question[count],next);
                    count++;
                  } else {
                    // console.log('finish');
                    location.href = 'lecturer_task_detail.html?u=' + $.urlParam('u');
                  }
                }
                next();

                /*
                 *

                for (i in question) {
                  var formDataQuestion = question[i];
                  console.log(formDataQuestion);
                }

                question.forEach(function (item) {
                  // console.log(item);
                  updateToServer(item,count);
                  count++;
                })

                 *
                 */
              }

              function updateToServer(formDataQuestion,next) {
                // console.log(formDataQuestion);
                $.ajax({
                  url: 'http://167.205.59.43:8012/popupquiz/postpopupquestion?u=' + $.urlParam('u'),
                  type: 'POST',
                  // async: false,
                  data: formDataQuestion,
                  // dataType: 'application/json', // what type of data do we expect back from the server
                  contentType: 'application/x-www-form-urlencoded',
                  success: function (res) {
                    console.log(res);
                    // console.log('question length: ' + question.length);
                    // console.log(this.ajaxI);
                    next();
                  },
                  error: function (err) {
                    // console.log('error: ', err);
                    alert('Submit Question #' + (i + 1) + ' Failed');
                  }
                });
              }

            });

          });

        });

        $('#classChoosenAss').on('change', function (evt) {

          $('#subTopicListAss').empty();

          /**
            * get sub topic
            */
          $.ajax({
            url: 'http://167.205.59.43:8011/courseclass/getsubtopicmeeting?courseOfferId=' + $(this).val(),
            type: 'GET',
            encode: true,
            success: function (res) {
              console.log(res);
              if (res.message.length > 0) {
              	for (var i = 0; i < res.message.length; i++) {
                  $('#subTopicListAss').append('<option name=\"' + res.message[i].courseSubTopicId + '\" value=\"' + res.message[i].courseSubTopicId + '\"> &nbsp;' + res.message[i].subTopicTitle + '</option>');
              	}
              }
            },
            error: function (err) {
              console.log('error: ', err);
              // alert('error: ', err);
            }
          })

        });

        $('#classChoosenQuiz').on('change', function (evt) {

          $('#subTopicListQuiz').empty();

          /**
            * get sub topic
            */
          $.ajax({
            url: 'http://167.205.59.43:8011/courseclass/getsubtopicmeeting?courseOfferId=' + $(this).val(),
            type: 'GET',
            encode: true,
            success: function (res) {
              console.log(res);
              if (res.message.length > 0) {
              	for (var i = 0; i < res.message.length; i++) {
                  $('#subTopicListQuiz').append('<option name=\"' + res.message[i].courseSubTopicId + '\" value=\"' + res.message[i].courseSubTopicId + '\"> &nbsp;' + res.message[i].subTopicTitle + '</option>');
              	}
              }
            },
            error: function (err) {
              console.log('error: ', err);
              // alert('error: ', err);
            }
          })

        });

      });

      /**
        * quiz
        */
      $('#questionnum').append('Question #' + (question.length + 1));

    });
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

  /**
    * tabs
    */
  $('#tabs').tabs();

  /**
    * accordion
    */
  $('#questioncontainer').accordion({
    collapsible: true
  });

  /**
    * datepicker
    */
  $("#start-datetime-local-input").datepicker({
  	dateFormat: "yy-mm-dd"
  });

  $("#end-datetime-local-input").datepicker({
  	dateFormat: "yy-mm-dd"
  });

})

function Base64DecodeUrl(str) {
  str = (str + '===').slice(0, str.length + (str.length % 4));
  return str.replace(/-/g, '+').replace(/_/g, '/');
}
