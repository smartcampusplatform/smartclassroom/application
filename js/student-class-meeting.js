$('document').ready(function () {

  /**
    * Link
    */
  $('#studentDashboardLink').on('click', function () {
    location.href = 'student_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_dashboard.html?u=' + $.urlParam('u');
  });
  $('#studentClassMeetingLink').on('click', function () {
    location.href = 'student_class_meeting.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_class_meeting.html?u=' + $.urlParam('u');
  });
  $('#studentAssignmentLink').on('click', function () {
    location.href = 'student_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_assignment.html?u=' + $.urlParam('u');
  });

  /**
    * cek room
    */
  if ($.urlParam('r')) {
    console.log('room: ' + $.urlParam('r'));
    // console.log($.urlParam('s'));
    // console.log(JSON.parse(atob(Base64DecodeUrl($.urlParam('s')))));
  } else {
    $('#contentContainer').hide();
  }

  /**
    * socket io
    */
  var socket = io('http://167.205.59.43:8012');
  setInterval(function () {
    if (!socket.connected) {
      // alert('disconnected');
      $('#connectionStat').removeClass('text-green');
      $('#connectionStat').removeClass('icon-checkbox-marked-circle');
      $('#connectionStat').addClass('text-red');
      $('#connectionStat').addClass('icon-close-circle');
    } else {
      $('#connectionStat').addClass('text-green');
      $('#connectionStat').addClass('icon-checkbox-marked-circle');
      $('#connectionStat').removeClass('text-red');
      $('#connectionStat').removeClass('icon-close-circle');
    }
    // alert("Hello");
  }, 10000);

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 1) {
            $('#userId').html(res.data.login_id);
  			    socket.emit('send-username', res.data.login_id, $.urlParam('r'));
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    }).done(function () {
      socket.on('updatechat', function (username, data) {
        // console.log('username: ', username);
        // console.log('data: ', data);
        $('#classDiscussion').append('(' + username + '): ' + data + '\n');
        document.getElementById("classDiscussion").scrollTop += 1000;
      });
      $('#submitMsg').on('click', function () {
        var msgText = $('#exampleFormControlDiscussion').val();
        // console.log(msgText);
        $('#classDiscussion').append('(You): ' + msgText + '\n');
        document.getElementById("classDiscussion").scrollTop += 1000;
        socket.emit('sendchat', msgText, $.urlParam('r'));
        $('#exampleFormControlDiscussion').val('');
      });
      $("#exampleFormControlDiscussion").keypress(function (e) {
        if (e.keyCode == 13) {
          e.preventDefault();
          // alert('You pressed enter!');
          var msgText = $('#exampleFormControlDiscussion').val();
          // console.log(msgText);
          $('#classDiscussion').append('(You): ' + msgText + '\n');
          document.getElementById("classDiscussion").scrollTop += 1000;
          socket.emit('sendchat', msgText, $.urlParam('r'));
          $('#exampleFormControlDiscussion').val('');
        }
      });
      socket.on('videolink', function (data) {
        console.log('video data: ', data);
        $('#presentationVideo').attr("src", "https://www.youtube.com/embed/" + data);
      });
    });
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

})

function Base64DecodeUrl(str) {
  str = (str + '===').slice(0, str.length + (str.length % 4));
  return str.replace(/-/g, '+').replace(/_/g, '/');
}
