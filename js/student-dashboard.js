$('document').ready(function () {

  /**
    * Link
    */
  $('#studentDashboardLink').on('click', function () {
    location.href = 'student_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_dashboard.html?u=' + $.urlParam('u');
  });
  $('#studentClassMeetingLink').on('click', function () {
    location.href = 'student_class_meeting.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_class_meeting.html?u=' + $.urlParam('u');
  });
  $('#studentAssignmentLink').on('click', function () {
    location.href = 'student_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_assignment.html?u=' + $.urlParam('u');
  });

  /**
    * socket io
    */
  var socket = io('http://167.205.59.43:8012');

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 1) {
            $('#userId').html(res.data.login_id);
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    })
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

  /**
    * dhtmlx scheduler
    */
  // scheduler.config.readonly = true;
  scheduler.config.multi_day = true;
  scheduler.config.xml_date = "%Y-%m-%d %H:%i";
  // scheduler.config.details_on_dblclick = true;
  scheduler.init('scheduler_here', new Date(), "week");
  // scheduler.load("../assets/dhtmlx/events.json", "json");
  // scheduler.parse([
  //   { text: "Meeting", start_date:"2019-08-11 14:00", end_date: "2019-08-11 17:00" },
  //   { text: "Conference", start_date:"2019-08-15 12:00", end_date: "2019-08-18 19:00" },
  //   { text: "Interview", start_date:"2019-08-24 09:00", end_date: "2019-08-24 10:00" }
  // ], "json");
  scheduler.load("http://167.205.59.43:8012/coursescheduling/getweeklyschedule?u=" + $.urlParam('u'), "json");

  scheduler.attachEvent("onDblClick", function(id, evt) {
    // any custom logic here
    // console.log(scheduler.getEvent(id));
    // $('#logoutModal').modal('toggle');
    // alert('event with id: ' + id);
    $('#btnReady').hide();
    $('#classTitle').html(scheduler.getEvent(id).text);
    $('#joinClassModal').modal('toggle');
    socket.emit('cekclass', scheduler.getEvent(id).id);
    socket.on('classstatus', function (data) {
      // console.log('username: ', username);
      console.log('data: ', data);
      if (data) {
        $('#btnReady').show();
      } else {
        $('#btnReady').hide();
      }

    });

    $('#btnReady').on('click', function () {
      // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_class_meeting.html?u=' + $.urlParam('u') + '&r=' + scheduler.getEvent(id).id + '&s=' + btoa(JSON.stringify(subTopicSelected));
      location.href = 'student_class_meeting.html?u=' + $.urlParam('u') + '&r=' + scheduler.getEvent(id).id;
    });

  })

})
