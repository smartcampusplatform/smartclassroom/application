(function () {
    $('document').ready(function () {
        // Prevent default for empty links
        var video = document.getElementById('video');
        navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
        var webcamStream;
        var socket = [null,null];

        $.ajax({
          url: "http://e-agriculture.net:50005/api/identification/type",
          type: "GET",
          success: function (result) {
            // Get result of identification
            var indexID = 1;
            console.log(result);
            // console.log(navigator.platform);

            // $('#identity-types').append('<div id=\"identitySelection\" class=\"selection-box item-wrap\">');
            for (var i = 0; i < result.length; i++) {
              if (i == 1) {
                $('#identity-types').append('<option name=\"' + result[i].ididentitytype + '\" value=\"type' + result[i].ididentitytype + '\"> &nbsp;' + result[i].description + '</option>');
                // $('#identitySelection').append('<a id=\"type' + result[i].ididentitytype + '\" href=\"#modal-' + result[i].ididentitytype + '\">' + result[i].description + '</a>');
              } else if (i > 1) {
                $('#identity-types').append('<option name=\"' + result[i].ididentitytype + '\" value=\"type' + result[i].ididentitytype + '\"> &nbsp;' + result[i].description + '</option>');
                // $('#identitySelection').append('<a id=\"type' + result[i].ididentitytype + '\" href=\"#modal-' + result[i].ididentitytype + '\">' + result[i].description + '</a>');
                socket.push(io('http://localhost:' + result[i].port));
                /*
                if (result[i].description.toString().toUpperCase().includes('CARD')) {
                  $('#top').append('<div id=\"modal-' + result[i].ididentitytype + '\" class=\"popup-modal slider mfp-hide\"><div class=\"description-box\"><div class=\"form-field\"><input name=\"reader' + result[i].ididentitytype + '\" type=\"text\" id=\"reader' + result[i].ididentitytype + '\" placeholder=\"UID CARD\" value=\"\" minlength=\"2\" required=\"\" readonly></div></div><div class=\"selection-box\"><a id=\"modal-' + result[i].ididentitytype + '-dismiss\" href=\"#\" class=\"popup-modal-dismiss\">Close</a></div></div>');
                }
                 */
              }
            }
            // $('#identity-types').append('</div>');
            for (var i = 2; i < result.length; i++) {
              // $('#type' + (i + 1)).hide();
              $('#type' + (i + 1)).prop('disabled', true);
            }
          },
          error: function () {
            $('#availableIdentityTypes').hide();
          }
        }).done(function () {

          /*----------------------------------------------------*/
        	/*	Check camera availability
        	------------------------------------------------------*/
          function detectWebcam(callback) {
            let md = navigator.mediaDevices;
            if (!md || !md.enumerateDevices) return callback(false);
            md.enumerateDevices().then(devices => {
              callback(devices.some(device => 'videoinput' === device.kind));
            });
          };
          detectWebcam(function (hasWebcam) {
            // console.log('Webcam: ' + (hasWebcam ? 'yes' : 'no'));
            if (!hasWebcam) {
              $('#type2').prop('disabled', true);
            } else {
              $('#type2').prop('disabled', false);
            }
          });

          /*----------------------------------------------------*/
        	/*	Check another identity type
        	------------------------------------------------------*/
          // console.log('socket length: ', socket.length);
          socket.forEach(function (item,index) {
            if (index > 1) {
              $('#type' + (index + 1)).click(function (evt) {
                // console.log(evt);
                // console.log($('.mfp-content'));
              });
              if (socket[index]) {
                socket[index].on('disconnect', function () {
                  // console.log('socket[' + index + ']: ', socket[index]);
                  $('#type' + (index + 1)).prop('disabled', true);
                })
                socket[index].on('connect', function () {
                  // console.log('socket[' + index + '], connected: ', socket[index]);
                  $('#type' + (index + 1)).prop('disabled', false);
                })
                socket[index].on('data', function (idx,data) {
                  // console.log('data: ', data);
                  $('#reader' + (idx + 1)).val(data);
                  var formIdentificationData = {
                    ididentitytype: (idx + 1),
                    atributvalue: data
                  }
                  // console.log(formIdentificationData);
                  $.ajax({
                    url: 'http://e-agriculture.net:50005/api/identification/data',
                    type: 'POST',
                    data: formIdentificationData,
                    // dataType: 'application/json', // what type of data do we expect back from the server
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (res) {
                      console.log(res);
                      if (res.userName) {
                        $('#loginFormUserId').val(res.nim);
                        $('#loginFormInputPassword').val(res.nim);
                      }
                    },
                    error: function (err) {
                      console.log('error: ', err);
                    }
                  }).done(function () {

                    /**
                     * Login user
                     */
                    var formDataLogin = {
                      'userid': $('input[id=loginFormUserId]').val(),
                      'userpass': $('input[id=loginFormInputPassword]').val()
                    };

                    // console.log(formDataLogin);
                    // alert(JSON.stringify(formDataLogin));

                    if ($('input[id=loginFormUserId]').val() && $('input[id=loginFormInputPassword]').val()) {
                      // console.log(formDataLogin);
                      $.ajax({
                        url: 'http://178.128.104.74/identitymanagement/api/login',
                        type: 'POST',
                        data: formDataLogin,
                        // dataType: 'application/json', // what type of data do we expect back from the server
                        contentType: 'application/x-www-form-urlencoded',
                        encode: true,
                        success: function (res) {
                          console.log(res);
                          // alert(JSON.stringify(res));
                          if (res.data) {
                            // alert('failed');
                            $.ajax({
                                url: 'http://178.128.104.74/identitymanagement/api/token/' + res.data.login_token,
                                type: 'GET',
                                encode: true,
                                success: function (result) {
                                  // console.log(result);
                                  // console.log(res.data.login_token);
                                  if (result.data.login_role == 1) {
                                    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                                    location.href = 'student_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                                  } else if (result.data.login_role == 2) {
                                    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                                    location.href = 'lecturer_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                                  }
                                },
                                error: function (error) {
                                  // console.log('error: ', error);
                                  // alert('error: ', error);
                                  alert('internal server error');
                                }
                            })
                          } 
                        },
                        error: function (err) {
                          // console.log('error: ', err);
                          // alert('error: ', err);
                          alert('internal server error');
                          // var locationRedirect;
                          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('internal server error');
                        }
                      });
                    };

                  });
                })
              }
            }
          })

          /*----------------------------------------------------*/
        	/*	Function to start webcam
        	------------------------------------------------------*/
          function startWebcam(next) {
            if (navigator.getUserMedia) {
              navigator.getUserMedia ({
                video: true,
                audio: false
              },
              // successCallback
              function (localMediaStream) {
                webcamStream = localMediaStream;
                video.srcObject = localMediaStream;
                setTimeout(function () {
                  next();
                }, 1000);
              },
              // errorCallback
              function(err) {
                console.log("The following error occured: " + err);
              });
            } else {
              console.log("getUserMedia is not supported");
            }
          };

          /*----------------------------------------------------*/
        	/*	Function to stop webcam
        	------------------------------------------------------*/
          function stopWebcam() {
            video.srcObject = null;
        	  webcamStream.getTracks().forEach(function(track) {
              track.stop();
            });
          }

          /*----------------------------------------------------*/
        	/*	Function to get video frame
        	------------------------------------------------------*/
          // returns a frame encoded in base64
          var sendFrame = () => {
            if (video.srcObject) {
              const canvas = document.createElement('canvas');
              canvas.width = video.videoWidth;
              canvas.height = video.videoHeight;
              canvas.getContext('2d').drawImage(video, 0, 0);
              const data = canvas.toDataURL('image/png');
              var formIdentificationData = {
                ididentitytype: 2,
                atributvalue: data
              }
              // console.log(formIdentificationData);
              $.ajax({
                url: 'http://e-agriculture.net:50005/api/identification/data',
                type: 'POST',
                data: formIdentificationData,
                // dataType: 'application/json', // what type of data do we expect back from the server
                contentType: 'application/x-www-form-urlencoded',
                success: function (res) {
                  console.log(res);
                  if (res.userName) {
                    $('#loginFormUserId').val(res.nim);
                    $('#loginFormInputPassword').val(res.nim);
                  } else {
                    sendFrame();
                  }
                },
                error: function (err) {
                  console.log('error: ', err);
                }
              }).done(function () {

                /**
                 * Login user
                 */
                var formDataLogin = {
                  'userid': $('input[id=loginFormUserId]').val(),
                  'userpass': $('input[id=loginFormInputPassword]').val()
                };

                // console.log(formDataLogin);
                // alert(JSON.stringify(formDataLogin));

                if ($('input[id=loginFormUserId]').val() && $('input[id=loginFormInputPassword]').val()) {
                  // console.log(formDataLogin);
                  $.ajax({
                    url: 'http://178.128.104.74/identitymanagement/api/login',
                    type: 'POST',
                    data: formDataLogin,
                    // dataType: 'application/json', // what type of data do we expect back from the server
                    contentType: 'application/x-www-form-urlencoded',
                    encode: true,
                    success: function (res) {
                      console.log(res);
                      // alert(JSON.stringify(res));
                      if (!res.data) {
                        // alert('failed');
                        sendFrame();
                        // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('failed');
                      } else {
                        // alert('success');
                        // localStorage.setItem('tokenuser', res.data.login_token);
                        // console.log(res.data.login_token);
                        $.ajax({
                            url: 'http://178.128.104.74/identitymanagement/api/token/' + res.data.login_token,
                            type: 'GET',
                            encode: true,
                            success: function (result) {
                              // console.log(result);
                              // console.log(res.data.login_token);
                              if(result.data.login_role == 1) {
                                // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                                location.href = 'student_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                              } else if(result.data.login_role == 2) {
                                // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                                location.href = 'lecturer_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                              }
                            },
                            error: function (error) {
                              // console.log('error: ', error);
                              // alert('error: ', error);
                              alert('internal server error');
                            }
                        })
                      }
                    },
                    error: function (err) {
                      // console.log('error: ', err);
                      // alert('error: ', err);
                      alert('internal server error');
                      // var locationRedirect;
                      // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('internal server error');
                    }
                  });
                };

              });
            }
          }

          /*----------------------------------------------------*/
        	/*	Face Recognition Identity Types click event
        	------------------------------------------------------*/

          $('#modal-2-dismiss').click(function (evt) {
            stopWebcam();
          });

          $('#availableIdentityTypes').on('click', function (evt) {
            $('#multiIdentityModal').modal('toggle');
          });

          $('#btnSelect').on('click', function (evt) {
            $('#multiIdentityModal').modal('toggle');
            // console.log($('#identity-types').val());
            $('#modal-' + $('#identity-types').val()[0].split('type')[1]).modal('toggle');
            if ($('#identity-types').val()[0].split('type')[1] == 2) {
              startWebcam(sendFrame);
            }
          });

        });

    });
})();
