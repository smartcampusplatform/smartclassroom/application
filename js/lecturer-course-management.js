$('document').ready(function () {

  /**
    * Link
    */
  $('#lecturerDashboardLink').on('click', function () {
    location.href = 'lecturer_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + $.urlParam('u');
  });
  $('#lecturerClassMeetingLink').on('click', function () {
    location.href = 'lecturer_class_meeting.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_meeting.html?u=' + $.urlParam('u');
  });
  $('#lecturerCreateAssignmentLink').on('click', function () {
    location.href = 'lecturer_create_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_create_assignment.html?u=' + $.urlParam('u');
  });
  $('#lecturerTaskDetailLink').on('click', function () {
    location.href = 'lecturer_task_detail.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_task_detail.html?u=' + $.urlParam('u');
  });
  $('#lecturerCourseManagementLink').on('click', function () {
    location.href = 'lecturer_course_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_management.html?u=' + $.urlParam('u');
  });
  $('#learningOutcomeManagementLink').on('click', function () {
    location.href = 'learning_outcome_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/learning_outcome_management.html?u=' + $.urlParam('u');
  });

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 2) {
            $('#userId').html(res.data.login_id);
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    });
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

  /**
    * Get Schedule
    */
  var courseOfferId = [];
  $.ajax({
    url: 'http://167.205.59.43:8012/coursescheduling/getweeklyschedule?u=' + $.urlParam('u'),
    type: 'GET',
    encode: true,
    success: function (res) {
      // console.log(res.data);
      // console.log(res.data.length);
      const map = new Map();
      for (const item of res.data) {
          if (!map.has(item.id.toString().split('CL')[0])) {
              map.set(item.id.toString().split('CL')[0], true); // set any value to Map
              courseOfferId.push({
                id: item.id.toString().split('CL')[0],
                name: item.text.toString().split('@')[0]
              });
          }
      }
      console.log(courseOfferId);
      // for (i in res.data) {
        // console.log(res.data[i].id.toString().split('CL')[0]);
      // }
    },
    error: function (err) {
      // console.log('error: ', err);
      // alert('error: ', err);
      alert('internal server error');
    }
  }).done(function () {

    for (var i = 0; i < courseOfferId.length; i++) {
      $('#courseOfferIdList').append('<option value=\"' + courseOfferId[i].id + '\">' + courseOfferId[i].name + '</option>');
    }

    $('#getTopic').click(function () {
      var topicURI = 'http://167.205.59.43:8011/contentmanagement/gettopic?courseOfferId=' + $('#courseOfferIdList').val();
      // console.log(topicURI);
      $.ajax({
        url: topicURI,
        type: 'GET',
        encode: true,
        success: function (res) {
          console.log(res);
          $('#myTableTopic').DataTable().destroy();
        },
        error: function (err) {
          // console.log('error: ', err);
          // alert('error: ', err);
          alert('internal server error');
        }
      }).done(function () {
        $('#myTableTopic').DataTable({
              "ajax": {
                "url": topicURI,
                "dataSrc": "message"
              },
              "deferRender": true,
              "columns": [
                  { "data": "topicOrdered" },
                  { "data": "courseTopicId" },
                  { "data": "topicTitle" },
                  { "data": "topicShortDescription" },
                  { "data": "createdBy" }
              ]
        });
      });
    });

    /*
    var next;
    // console.log('question: ' + JSON.stringify(question));
    if (courseOfferId.length > 0) {
      var count = 0
      next = function () {
        if (count < courseOfferId.length) {
          getTopic(courseOfferId[count].id,next);
          count++;
        }
      }
      next();
    }

    function getTopic(courseOfferId,next) {
      // console.log(formDataQuestion);
      $.ajax({
        url: 'http://167.205.59.43:8011/contentmanagement/gettopic?courseOfferId=' + courseOfferId,
        type: 'GET',
        encode: true,
        success: function (res) {
          console.log(res);
          // console.log('question length: ' + question.length);
          // console.log(this.ajaxI);
          for (i in res.message) {
            $('#topicList').append('<li>' + res.message[i].topicTitle + '</li>');
          }
          next();
        },
        error: function (err) {
          // console.log('error: ', err);
          alert('Get Topic Failed');
        }
      });
    }
     */

  });

  /**
    * tabs
    */
  $('#tabs').tabs();

})
