$('document').ready(function () {

  /**
    * Link
    */
  $('#lecturerDashboardLink').on('click', function () {
    location.href = 'lecturer_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + $.urlParam('u');
  });
  $('#lecturerClassMeetingLink').on('click', function () {
    location.href = 'lecturer_class_meeting.html?u=' + $.urlParam('u') + '&r=' + $.urlParam('r') + '&s='+ $.urlParam('s');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_meeting.html?u=' + $.urlParam('u');
  });
  $('#lecturerCreateAssignmentLink').on('click', function () {
    location.href = 'lecturer_create_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_create_assignment.html?u=' + $.urlParam('u');
  });
  $('#lecturerTaskDetailLink').on('click', function () {
    location.href = 'lecturer_task_detail.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_task_detail.html?u=' + $.urlParam('u');
  });
  $('#lecturerCourseManagementLink').on('click', function () {
    location.href = 'lecturer_course_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_management.html?u=' + $.urlParam('u');
  });
  $('#learningOutcomeManagementLink').on('click', function () {
    location.href = 'learning_outcome_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/learning_outcome_management.html?u=' + $.urlParam('u');
  });

  /**
    * cek room
    */
  if ($.urlParam('r')) {
    console.log('room: ' + $.urlParam('r'));
    console.log($.urlParam('s'));
    console.log(JSON.parse(atob(Base64DecodeUrl($.urlParam('s')))));
  } else {
    $('#contentContainer').hide();
  }

  /**
    * socket io
    */
  var socket = io('http://167.205.59.43:8012');
  setInterval(function () {
    if (!socket.connected) {
      // alert('disconnected');
      $('#connectionStat').removeClass('text-green');
      $('#connectionStat').removeClass('icon-checkbox-marked-circle');
      $('#connectionStat').addClass('text-red');
      $('#connectionStat').addClass('icon-close-circle');
    } else {
      $('#connectionStat').addClass('text-green');
      $('#connectionStat').addClass('icon-checkbox-marked-circle');
      $('#connectionStat').removeClass('text-red');
      $('#connectionStat').removeClass('icon-close-circle');
    }
    // alert("Hello");
  }, 10000);

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 2) {
            $('#userId').html(res.data.login_id);
  			    socket.emit('send-username', res.data.login_id, $.urlParam('r'));
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    }).done(function () {
      socket.on('updatechat', function (username, data) {
        // console.log('username: ', username);
        // console.log('data: ', data);
        $('#classDiscussion').append('(' + username + '): ' + data + '\n');
        document.getElementById("classDiscussion").scrollTop += 1000;
      });
      $('#submitMsg').on('click', function () {
        var msgText = $('#exampleFormControlDiscussion').val();
        // console.log(msgText);
        $('#classDiscussion').append('(You): ' + msgText + '\n');
        document.getElementById("classDiscussion").scrollTop += 1000;
        socket.emit('sendchat', msgText, $.urlParam('r'));
        $('#exampleFormControlDiscussion').val('');
      });
      $("#exampleFormControlDiscussion").keypress(function (e) {
        if (e.keyCode == 13) {
          e.preventDefault();
          // alert('You pressed enter!');
          var msgText = $('#exampleFormControlDiscussion').val();
          // console.log(msgText);
          $('#classDiscussion').append('(You): ' + msgText + '\n');
          document.getElementById("classDiscussion").scrollTop += 1000;
          socket.emit('sendchat', msgText, $.urlParam('r'));
          $('#exampleFormControlDiscussion').val('');
        }
      });
      $('#shareLink').on('click', function () {
        var videoLink = $('#videoFileLink').val();
        // console.log(videoLink);
        socket.emit('sharedlink', videoLink, $.urlParam('r'));
        $('#presentationVideo').attr("src", "https://www.youtube.com/embed/" + videoLink.split('/')[videoLink.split('/').length - 1]);
      });
    });
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

})

function Base64DecodeUrl(str) {
  str = (str + '===').slice(0, str.length + (str.length % 4));
  return str.replace(/-/g, '+').replace(/_/g, '/');
}
