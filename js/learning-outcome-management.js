$('document').ready(function () {

  /**
    * Link
    */
  $('#lecturerDashboardLink').on('click', function () {
    location.href = 'lecturer_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + $.urlParam('u');
  });
  $('#lecturerClassMeetingLink').on('click', function () {
    location.href = 'lecturer_class_meeting.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_meeting.html?u=' + $.urlParam('u');
  });
  $('#lecturerCreateAssignmentLink').on('click', function () {
    location.href = 'lecturer_create_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_create_assignment.html?u=' + $.urlParam('u');
  });
  $('#lecturerTaskDetailLink').on('click', function () {
    location.href = 'lecturer_task_detail.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_task_detail.html?u=' + $.urlParam('u');
  });
  $('#lecturerCourseManagementLink').on('click', function () {
    location.href = 'lecturer_course_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_management.html?u=' + $.urlParam('u');
  });
  $('#learningOutcomeManagementLink').on('click', function () {
    location.href = 'learning_outcome_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/learning_outcome_management.html?u=' + $.urlParam('u');
  });

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 2) {
            $('#userId').html(res.data.login_id);
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    }).done(function () {
      // console.log($('#userId').html());
      $.ajax({
        url: 'http://167.205.59.43:8011/courseclass/getcourseclass?staffId=' + $('#userId').html(),
        type: 'GET',
        encode: true,
        success: function (res) {
          console.log(res);
          if (res.message.length > 0) {
            for (var i = 0; i < res.message.length; i++) {
              $('#courseChoosen').append('<option name=\"' + res.message[i].courseOfferId + '\" value=\"' + res.message[i].courseOfferId + '\"> &nbsp;' + res.message[i].courseNameId + '</option>');
        	  }
          }
        },
        error: function (err) {
          // console.log('error: ', err);
          // alert('error: ', err);
          alert('internal server error');
        }
      }).done(function () {

        $('#getLearningOutcome').click(function () {
          // $('#outcomeList').empty();
          // console.log('Period: ' + $('#periodChoosen').val());
          // console.log('Course: ' + $('#courseChoosen').val());
          var learningOutcomeURI = 'http://167.205.59.43:8012/learningoutcomemanagement/learningoutcome?u=' + $.urlParam('u') + '&courseOfferId=' + $('#courseChoosen').val();
          if ($('#periodChoosen').val() != 'NULL') {
            learningOutcomeURI += '&learningPeriodId=' + $('#periodChoosen').val();
          }
          // console.log(learningOutcomeURI);
          $.ajax({
            url: learningOutcomeURI,
            type: 'GET',
            encode: true,
            success: function (res) {
              // console.log(res);
              $('#myTableLearningOutcome').DataTable().destroy();
            },
            error: function (err) {
              // console.log('error: ', err);
              // alert('error: ', err);
              alert('internal server error');
            }
          }).done(function () {

            $('#myTableLearningOutcome').DataTable({
                  "ajax": learningOutcomeURI,
                  "deferRender": true,
                  "columns": [
                      { "data": "learningOutcomeId" },
                      { "data": "activities" },
                      { "data": "description" },
                      { "data": "difficultyLevel" }
                  ]
            });

          });

        })

      });
    });

    $.ajax({
      url: 'http://167.205.59.43:8011/academicagendasetup/getlearningperiod?list=true',
      type: 'GET',
      encode: true,
      success: function (res) {
        console.log(res);
        if (res.message.length > 0) {
          for (var i = (res.message.length - 1); i >= 0; i--) {
            $('#periodChoosen').append('<option name=\"' + res.message[i].learningPeriodId + '\" value=\"' + res.message[i].learningPeriodId + '\"> &nbsp;' + res.message[i].year  + ' Semester ' + res.message[i].semester + '</option>');
          }
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    });
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

  /**
    * tabs
    */
  $('#tabs').tabs();

})
