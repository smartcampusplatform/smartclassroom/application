$('document').ready(function () {

  /**
    * Link
    */
  $('#lecturerDashboardLink').on('click', function () {
    location.href = 'lecturer_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + $.urlParam('u');
  });
  $('#lecturerClassMeetingLink').on('click', function () {
    location.href = 'lecturer_class_meeting.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_meeting.html?u=' + $.urlParam('u');
  });
  $('#lecturerCreateAssignmentLink').on('click', function () {
    location.href = 'lecturer_create_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_create_assignment.html?u=' + $.urlParam('u');
  });
  $('#lecturerTaskDetailLink').on('click', function () {
    location.href = 'lecturer_task_detail.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_task_detail.html?u=' + $.urlParam('u');
  });
  $('#lecturerCourseManagementLink').on('click', function () {
    location.href = 'lecturer_course_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_management.html?u=' + $.urlParam('u');
  });
  $('#learningOutcomeManagementLink').on('click', function () {
    location.href = 'learning_outcome_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/learning_outcome_management.html?u=' + $.urlParam('u');
  });

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 2) {
            $('#userId').html(res.data.login_id);
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    })
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

  /**
    * dhtmlx scheduler
    */
  // scheduler.config.readonly = true;
  scheduler.config.multi_day = true;
  scheduler.config.xml_date = "%Y-%m-%d %H:%i";
  // scheduler.config.details_on_dblclick = true;
  scheduler.init('scheduler_here', new Date(), "week");
  // scheduler.parse([
  //   { text: "Meeting", start_date:"2019-08-11 14:00", end_date: "2019-08-11 17:00" },
  //   { text: "Conference", start_date:"2019-08-15 12:00", end_date: "2019-08-18 19:00" },
  //   { text: "Interview", start_date:"2019-08-24 09:00", end_date: "2019-08-24 10:00" }
  // ], "json");
  scheduler.load("http://167.205.59.43:8012/coursescheduling/getweeklyschedule?u=" + $.urlParam('u'), "json");

  scheduler.attachEvent("onDblClick", function(id, evt) {
    // any custom logic here
    // console.log(scheduler.getEvent(id));
    $('#classDetail').empty();
    console.log(scheduler.getEvent(id));
    console.log(scheduler.getEvent(id).id.split('CL')[0]);
    $('#classTitle').html(scheduler.getEvent(id).text);
    $.ajax({
      url: 'http://167.205.59.43:8011/courseclass/getsubtopicmeeting?courseOfferId=' + scheduler.getEvent(id).id.split('CL')[0],
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if(res.message.length > 0) {
          // console.log(res.message);
        	// $('#classDetail').append('Choose Sub Topic:<br>');
        	for(var i = 0; i < res.message.length; i++) {
            $('#classDetail').append('<option name=\"' + res.message[i].courseSubTopicId + '\" value=\"' + res.message[i].courseSubTopicId + '\"> &nbsp;' + res.message[i].subTopicTitle + '</option>');
        	  // $('#classDetail').append('<input type=\"checkbox\" class=\"subTopicList\" name=\"' + res.message[i].courseSubTopicId + '\" value=\"' + res.message[i].courseSubTopicId + '\"> &nbsp;' + res.message[i].subTopicTitle + '<br>');
        	}
          $('#classDetail').show();
          // $('#btnReady').show();
        } else {
          $('#classDetail').hide();
          // $('#btnReady').hide();
        }
      },
      error: function (err) {
        console.log('error: ', err);
        // alert('error: ', err);
      }
    }).done(function () {
      $('#btnReady').on('click', function () {
        var subTopicSelected = []
	      $('#classDetail').each(function() {
          console.log($(this).val());
	        subTopicSelected.push($(this).val());
       	});
		    console.log(JSON.stringify(subTopicSelected));
		    console.log(Base64EncodeUrl(btoa(JSON.stringify(subTopicSelected))));
        // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_meeting.html?u=' + $.urlParam('u') + '&r=' + scheduler.getEvent(id).id + '&s=' + btoa(JSON.stringify(subTopicSelected));
        location.href = 'lecturer_class_meeting.html?u=' + $.urlParam('u') + '&r=' + scheduler.getEvent(id).id + '&s=' + Base64EncodeUrl(btoa(JSON.stringify(subTopicSelected)));
      });
	    // $('#classDetail').html('START: ' + scheduler.getEvent(id).start_date);
      // alert(JSON.stringify(scheduler.getEvent(id)));
      $('#initiateClassModal').modal('toggle');
      // alert('event with id: ' + id)
    });

  });

})

function Base64EncodeUrl(str) {
  return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/\=+$/, '');
}
