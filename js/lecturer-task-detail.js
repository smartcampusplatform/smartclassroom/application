$('document').ready(function () {

  /**
    * Link
    */
  $('#lecturerDashboardLink').on('click', function () {
    location.href = 'lecturer_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + $.urlParam('u');
  });
  $('#lecturerClassMeetingLink').on('click', function () {
    location.href = 'lecturer_class_meeting.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_meeting.html?u=' + $.urlParam('u');
  });
  $('#lecturerCreateAssignmentLink').on('click', function () {
    location.href = 'lecturer_create_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_create_assignment.html?u=' + $.urlParam('u');
  });
  $('#lecturerTaskDetailLink').on('click', function () {
    location.href = 'lecturer_task_detail.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_task_detail.html?u=' + $.urlParam('u');
  });
  $('#lecturerCourseManagementLink').on('click', function () {
    location.href = 'lecturer_course_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_course_management.html?u=' + $.urlParam('u');
  });
  $('#learningOutcomeManagementLink').on('click', function () {
    location.href = 'learning_outcome_management.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/learning_outcome_management.html?u=' + $.urlParam('u');
  });

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 2) {
            $('#userId').html(res.data.login_id);
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    });
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

  /**
    * tabs
    */
  $('#tabs').tabs();

})
