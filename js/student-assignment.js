$('document').ready(function () {

  /**
    * Link
    */
  $('#studentDashboardLink').on('click', function () {
    location.href = 'student_dashboard.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_dashboard.html?u=' + $.urlParam('u');
  });
  $('#studentClassMeetingLink').on('click', function () {
    location.href = 'student_class_meeting.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_class_meeting.html?u=' + $.urlParam('u');
  });
  $('#studentAssignmentLink').on('click', function () {
    location.href = 'student_assignment.html?u=' + $.urlParam('u');
    // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_assignment.html?u=' + $.urlParam('u');
  });

  /**
    * cek room
    */
  if ($.urlParam('r')) {
    console.log('room: ' + $.urlParam('r'));
    // console.log($.urlParam('s'));
    // console.log(JSON.parse(atob(Base64DecodeUrl($.urlParam('s')))));
  } else {
    $('#contentContainer').hide();
  }

  /**
    * Cek user
    */
  if ($.urlParam('u')) {
    $.ajax({
      url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
      type: 'GET',
      encode: true,
      success: function (res) {
        // console.log(res);
        if (res.data) {
          if (res.data.login_role == 1) {
            $('#userId').html(res.data.login_id);
          } else {
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
            location.href = 'index.html?m=' + encodeURIComponent('not found');
          }
        } else {
          // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
          location.href = 'index.html?m=' + encodeURIComponent('not found');
        }
      },
      error: function (err) {
        // console.log('error: ', err);
        // alert('error: ', err);
        alert('internal server error');
      }
    });
  } else {
    // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('not found');
    location.href = 'index.html?m=' + encodeURIComponent('not found');
  }

  /**
    * tabs
    */
  $('#tabs').tabs();

})

function Base64DecodeUrl(str) {
  str = (str + '===').slice(0, str.length + (str.length % 4));
  return str.replace(/-/g, '+').replace(/_/g, '/');
}
