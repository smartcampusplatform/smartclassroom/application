(function () {
  $('document').ready(function () {

      // Prevent default for empty links
      $('[href="#"]').click(function (e) {
        e.preventDefault()
      });

      // Adding data-parent to togglable nav items
      $('#sidenav [data-toggle]').each(function () {
        $(this).attr('data-parent', '#' + $(this).parent().parent().attr('id'));
      });

      /**
        * Trigger window resize to update nvd3 charts
        */
      $('[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        window.dispatchEvent(new Event('resize'));
      });

      /**
        * Enable tooltips everywhere
        */
      $('[data-toggle="tooltip"]').tooltip();

      /**
        * Enable popovers everywhere
        */
      $('[data-toggle="popover"]').popover();

      // Activate animated progress bar
      $('.bd-toggle-animated-progress').on('click', function () {
        $(this).siblings('.progress').find('.progress-bar-striped').toggleClass('progress-bar-animated')
      });

      /**
        * Enable Custom Scrollbars only for desktop
        */
      var mobileDetect = new MobileDetect(window.navigator.userAgent);

      if (!mobileDetect.mobile()) {
        $('.custom-scrollbar').each(function () {
          new PerfectScrollbar(this);
        })
      } else {
        $('body').addClass('is-mobile');
      }

      /**
        * Fix code block indentations
        */
      $('code').each(function () {
        var lines = $(this).html().split('\n');

        if (lines[0] === '') {
          lines.shift()
        }

        var matches;
        var indentation = (matches = /^\s+/.exec(lines[0])) !== null ? matches[0] : null;

        if (!!indentation) {
          lines = lines.map(function (line) {
            return line.replace(indentation, '');
          });

          $(this).html(lines.join('\n').trim());
        }
      });

      /**
        * Flip source-preview cards
        */
      $('.toggle-source-preview').on('click', function () {
        $(this).parents('.example').toggleClass('show-source');
      });

      /**
        * Get URL Param
        */
      $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
          return null;
        }
        return decodeURI(results[1]) || 0;
      };

      /**
        * Logout user
        */
      $("#btnLogout").on('click', function (evt) {

        var formLogout = {
          'usertoken': $.urlParam('u')
        };

        // console.log(formLogout);
        // alert(JSON.stringify(formLogout));

        $.ajax({
          url: 'http://178.128.104.74/identitymanagement/api/logout',
          type: 'POST',
          data: formLogout,
          // dataType: 'application/json', // what type of data do we expect back from the server
          contentType: 'application/x-www-form-urlencoded',
          encode: true,
          success: function (res) {
            // console.log(res);
            // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('logout success');
            location.href = 'index.html?m=' + encodeURIComponent('logout success');
          },
          error: function (err) {
            // console.log('error: ', err);
            alert('internal server error');
          }
        });

      });

  });

  /**
    * Data tables fix header resize
    */
  $(window).on('resize', function () {
    $.fn.dataTable.tables({
      visible: true,
      api    : true
    }).columns.adjust();
  });

  /**
    * PNotify default styling.
    */
  PNotify.defaults.styling = 'material';
  PNotify.defaults.icons = 'material';

})();
