(function () {
    $('document').ready(function () {
        // Prevent default for empty links
        $('[href="#"]').click(function (e) {
            e.preventDefault()
        });

        // Adding data-parent to togglable nav items
        $('#sidenav [data-toggle]').each(function () {
            $(this).attr('data-parent', '#' + $(this).parent().parent().attr('id'));
        });

        /**
         * Trigger window resize to update nvd3 charts
         */
        $('[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            window.dispatchEvent(new Event('resize'));
        });

        /**
         * Enable tooltips everywhere
         */
        $('[data-toggle="tooltip"]').tooltip();

        /**
         * Enable popovers everywhere
         */
        $('[data-toggle="popover"]').popover();

        // Activate animated progress bar
        $('.bd-toggle-animated-progress').on('click', function () {
            $(this).siblings('.progress').find('.progress-bar-striped').toggleClass('progress-bar-animated')
        });

        /**
         * Enable Custom Scrollbars only for desktop
         */
        var mobileDetect = new MobileDetect(window.navigator.userAgent);

        if (!mobileDetect.mobile()) {
            $('.custom-scrollbar').each(function () {
                new PerfectScrollbar(this);
            })
        } else {
            $('body').addClass('is-mobile');
        }

        /**
         * Fix code block indentations
         */
        $('code').each(function () {
            var lines = $(this).html().split('\n');

            if (lines[0] === '') {
                lines.shift()
            }

            var matches;
            var indentation = (matches = /^\s+/.exec(lines[0])) !== null ? matches[0] : null;

            if (!!indentation) {
                lines = lines.map(function (line) {
                    return line.replace(indentation, '');
                });

                $(this).html(lines.join('\n').trim());
            }
        });

        /**
         * Flip source-preview cards
         */
        $('.toggle-source-preview').on('click', function () {
            $(this).parents('.example').toggleClass('show-source');
        });

        /**
         * Get URL Param
         */
        $.urlParam = function (name) {
          var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
          if (results == null) {
             return null;
          }
          return decodeURI(results[1]) || 0;
        };

        /**
         * Cek user
         */
        if ($.urlParam('u')) {
          $.ajax({
              url: 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u'),
              type: 'GET',
              encode: true,
              success: function (res) {
                // console.log(res);
                if (res.data.login_role == 1) {
                  // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_dashboard.html?u=' + encodeURIComponent($.urlParam('u'));
                  location.href = 'student_dashboard.html?u=' + encodeURIComponent($.urlParam('u'));
                } else if (res.data.login_role == 2) {
                  // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + encodeURIComponent($.urlParam('u'));
                  location.href = 'lecturer_dashboard.html?u=' + encodeURIComponent($.urlParam('u'));
                }
              },
              error: function (err) {
                // console.log('error: ', err);
                // alert('error: ', err);
                alert('internal server error');
              }
          });
        }

        /**
         * Login user
         */
        $("#loginBtn").on('click', function (evt) {

          evt.preventDefault();

          var formDataLogin = {
            'userid': $('input[id=loginFormUserId]').val(),
            'userpass': $('input[id=loginFormInputPassword]').val()
          };

          // console.log(formDataLogin);
          // alert(JSON.stringify(formDataLogin));

          if ($('input[id=loginFormUserId]').val() && $('input[id=loginFormInputPassword]').val()) {
            // console.log(formDataLogin);
            $.ajax({
              url: 'http://178.128.104.74/identitymanagement/api/login',
              type: 'POST',
              data: formDataLogin,
              // dataType: 'application/json', // what type of data do we expect back from the server
              contentType: 'application/x-www-form-urlencoded',
              encode: true,
              success: function (res) {
                console.log(res);
                // alert(JSON.stringify(res));
                if (!res.data) {
                  alert('failed');
                  // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('failed');
                } else {
                  // alert('success');
                  // localStorage.setItem('tokenuser', res.data.login_token);
                  // console.log(res.data.login_token);
                  $.ajax({
                      url: 'http://178.128.104.74/identitymanagement/api/token/' + res.data.login_token,
                      type: 'GET',
                      encode: true,
                      success: function (result) {
                        // console.log(result);
                        // console.log(res.data.login_token);
                        if(result.data.login_role == 1) {
                          // location.href = 'http://178.128.104.74/smartclassroomapp/views/student_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                          location.href = 'student_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                        } else if(result.data.login_role == 2) {
                          // location.href = 'http://178.128.104.74/smartclassroomapp/views/lecturer_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                          location.href = 'lecturer_dashboard.html?u=' + encodeURIComponent(res.data.login_token);
                        }
                      },
                      error: function (error) {
                        // console.log('error: ', error);
                        // alert('error: ', error);
                        alert('internal server error');
                      }
                  })
                }
              },
              error: function (err) {
                // console.log('error: ', err);
                // alert('error: ', err);
                alert('internal server error');
                // var locationRedirect;
                // location.href = 'http://178.128.104.74/smartclassroomapp?m=' + encodeURIComponent('internal server error');
              }
            });
          };

        });

    });

})();
