(function () {
    $('document').ready(function () {

        // Prevent default for empty links
        $('[href="#"]').click(function (e) {
            e.preventDefault();
        });

        // Adding data-parent to togglable nav items
        $('#sidenav [data-toggle]').each(function () {
            $(this).attr('data-parent', '#' + $(this).parent().parent().attr('id'));
        });

        /**
         * Trigger window resize to update nvd3 charts
         */
        $('[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            window.dispatchEvent(new Event('resize'));
        });

        /**
         * Enable tooltips everywhere
         */
        $('[data-toggle="tooltip"]').tooltip();

        /**
         * Enable popovers everywhere
         */
        $('[data-toggle="popover"]').popover();

        // Activate animated progress bar
        $('.bd-toggle-animated-progress').on('click', function () {
            $(this).siblings('.progress').find('.progress-bar-striped').toggleClass('progress-bar-animated')
        });

        /**
         * Enable Custom Scrollbars only for desktop
         */
        var mobileDetect = new MobileDetect(window.navigator.userAgent);

        if (!mobileDetect.mobile()) {
            $('.custom-scrollbar').each(function () {
                new PerfectScrollbar(this);
            })
        } else {
            $('body').addClass('is-mobile');
        }

        /**
         * Fix code block indentations
         */
        $('code').each(function () {
            var lines = $(this).html().split('\n');

            if (lines[0] === '') {
                lines.shift()
            }

            var matches;
            var indentation = (matches = /^\s+/.exec(lines[0])) !== null ? matches[0] : null;

            if (!!indentation) {
                lines = lines.map(function (line) {
                    return line.replace(indentation, '');
                });

                $(this).html(lines.join('\n').trim());
            }
        });

        /**
         * Flip source-preview cards
         */
        $('.toggle-source-preview').on('click', function () {
            $(this).parents('.example').toggleClass('show-source');
        });

        /**
         * Get URL Param
         */
        $.urlParam = function (name) {
          var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
          if (results == null) {
             return null;
          }
          return decodeURI(results[1]) || 0;
        };

        /**
         * socket io
         */
        var socket = io('http://167.205.59.43:8012');
        socket.emit('send-username', 'admin');
        socket.on('updatechat', function (username, data) {
      		// $('#conversation').append('<b>'+username + ':</b> ' + data + '<br>');
          // console.log('username: ', username);
          // console.log('data: ', data);
          $('#classDiscussion').append('(' + username + '): ' + data + '\n');
          document.getElementById("classDiscussion").scrollTop += 1000;
      	});
        socket.on('joineduser', function (data) {
          // console.log('username: ', username);
          console.log('joined user: ', data);
      	});
        $('#submitMsg').on('click', function () {
          var msgText = $('#exampleFormControlDiscussion').val();
          // console.log(msgText);
          $('#classDiscussion').append('(You): ' + msgText + '\n');
          document.getElementById("classDiscussion").scrollTop += 1000;
          socket.emit('sendchat', msgText);
          $('#exampleFormControlDiscussion').val('');
        });
        $("#exampleFormControlDiscussion").keypress(function (e) {
          if (e.keyCode == 13) {
            e.preventDefault();
            // alert('You pressed enter!');
            var msgText = $('#exampleFormControlDiscussion').val();
            // console.log(msgText);
            $('#classDiscussion').append('(You): ' + msgText + '\n');
            document.getElementById("classDiscussion").scrollTop += 1000;
            socket.emit('sendchat', msgText);
            $('#exampleFormControlDiscussion').val('');
          }
        });
        $('#shareLink').on('click', function () {
          var videoLink = $('#videoFileLink').val();
          // console.log(videoLink);
          socket.emit('sharedlink', videoLink);
          $('#presentationVideo').attr("src", "https://www.youtube.com/embed/" + videoLink);
        });
        socket.on('videolink', function (data) {
          console.log('video data: ', data);
          $('#presentationVideo').attr("src", "https://www.youtube.com/embed/" + data);
      	});

    });

    /**
     * Data tables fix header resize
     */
    $(window).on('resize', function () {
        $.fn.dataTable.tables({
            visible: true,
            api    : true
        }).columns.adjust();
    });

    /**
     *     PNotify default styling.
     */
    PNotify.defaults.styling = 'material';
    PNotify.defaults.icons = 'material';

})();
