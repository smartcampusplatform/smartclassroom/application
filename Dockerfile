FROM php:7.2-apache

RUN apt-get update 

COPY . /var/www/html
COPY vhost.conf /etc/apache2/sites-available/000-default.conf

RUN chown -R www-data:www-data /var/www \
    && a2enmod rewrite

RUN service apache2 restart

EXPOSE 80
